//
//  GIFListItemRowController.swift
//  GIFViewer
//
//  Created by Daniel Vera on 7/12/15.
//  Copyright © 2015 chuwito. All rights reserved.
//

import WatchKit

class GIFListItemRowController: NSObject {

    @IBOutlet var GIFName: WKInterfaceLabel!
    
    var frameName: String = ""
    var frameCount: Int = 0
}
