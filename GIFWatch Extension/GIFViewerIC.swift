//
//  GIFViewerIC.swift
//  GIFViewer
//
//  Created by Daniel Vera on 7/12/15.
//  Copyright © 2015 chuwito. All rights reserved.
//

import WatchKit
import Foundation


class GIFViewerIC: WKInterfaceController {

    @IBOutlet var GIFImage: WKInterfaceImage!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        let gifInfo = context as! GIFListItemRowController
        GIFImage.setImageNamed(gifInfo.frameName)
        
        let animationDuration:Double = Double(gifInfo.frameCount) * 0.15
        GIFImage.startAnimatingWithImagesInRange(NSRange(location: 0, length: gifInfo.frameCount)
            , duration: animationDuration
            , repeatCount: 10)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func willDisappear() {
        GIFImage.stopAnimating()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
