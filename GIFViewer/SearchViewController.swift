//
//  SearchViewController.swift
//  GIFViewer
//
//  Created by Daniel Vera on 4/2/16.
//  Copyright © 2016 chuwito. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var resultsCollectionView: UICollectionView!
    @IBOutlet weak var gifSearchBar:UISearchBar!
    @IBOutlet weak var errorLabel:UILabel!
    
    let kGIFCellId = "gifCellId"
    let kCellSpacing:CGFloat = 5.0
    var giphyResults = [AnyObject]()
    var screenSize:CGRect = CGRectZero
    var cellSize:CGFloat = 200
    
    var selectedGIFName = ""
    var selectedGIFData:NSData! = NSData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenSize = UIScreen.mainScreen().bounds
        cellSize = (screenSize.width - kCellSpacing) * 0.5
        
        AXCGiphy.setGiphyAPIKey(kGiphyPublicAPIKey)
        searchGIFWithName("pokemon")
    }
    
    override func viewWillAppear(animated: Bool) {
        self.addSearchBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchGIFWithName(gifname: String)
    {
        AXCGiphy.searchGiphyWithTerm(gifname, limit: 20, offset: 0) { (results:[AnyObject]!, error: NSError!) in
            if (error == nil) {
                self.giphyResults = results
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.resultsCollectionView.hidden = false
                    self.resultsCollectionView.reloadData()
                })
                return
            }
            //error != nil
            if let errorMsj = error.userInfo["error_message"] {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.resultsCollectionView.hidden = true
                    self.errorLabel.text = errorMsj as? String
                    self.giphyResults = [AnyObject]()
                    self.resultsCollectionView.reloadData()
                })
            }
        }
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval)
    {
        var screenWidth:CGFloat = screenSize.width
        if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
            screenWidth = screenSize.height
        }
        cellSize = (screenWidth - kCellSpacing) * 0.5
        resultsCollectionView.reloadData()
    }
    
    func showOptionsActionSheet()
    {
        let optionMenu = UIAlertController(title: nil, message: "Select one option:",
                                           preferredStyle: .ActionSheet)
        
        let saveAction = UIAlertAction(title:"Save GIF", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.saveGIF()
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func saveGIF() {
        if (selectedGIFData != nil) {
            var documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            documentsPath += "/gifsFiles/" + selectedGIFName
            //try! selectedGIFData.writeToFile(documentsPath, options: NSDataWritingOptions.DataWritingAtomic)
            
            print("GIF Saved: \(documentsPath)")
        }
    }
    
    //MARK: Search Bar
    func addSearchBar() {
        self.gifSearchBar.searchBarStyle = UISearchBarStyle.Minimal
        self.gifSearchBar.tintColor = UIColor.whiteColor()
        self.gifSearchBar.barTintColor = UIColor.whiteColor()
        self.gifSearchBar.delegate = self;
        self.gifSearchBar.placeholder = "search here";
        let textFieldInsideSearchBar = self.gifSearchBar.valueForKey("searchField") as? UITextField
        if (textFieldInsideSearchBar != nil) {
            textFieldInsideSearchBar?.textColor = UIColor.whiteColor()
        }
    }
    
}

extension SearchViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return giphyResults.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell:GIFCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(kGIFCellId, forIndexPath: indexPath)
            as! GIFCollectionViewCell
        
        let gif: AXCGiphy = giphyResults[indexPath.row] as! AXCGiphy
        
        let request = NSURLRequest(URL: gif.originalImage.url)
        //print("URL: \(gif.originalImage.url)")
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) in
            if (error == nil && data != nil) {
                let gifImage = UIImage(data: data!)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    cell.gifView.image = gifImage
                })
            }
        }.resume()
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        if let gif:AXCGiphy = giphyResults[indexPath.row] as? AXCGiphy
        {
            self.selectedGIFName = gif.originalImage.url.absoluteString
            let cell:GIFCollectionViewCell = self.resultsCollectionView.cellForItemAtIndexPath(indexPath) as! GIFCollectionViewCell
            self.selectedGIFData = UIImageAnimatedGIFRepresentation(cell.gifView.image)
            
            self.showOptionsActionSheet()
        }
    }
    
}

extension SearchViewController: UISearchBarDelegate
{
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if (searchText.characters.count > 0) {
            self.searchGIFWithName(searchText)
            return
        }
        // searchText.characters.count == 0
        giphyResults = [AnyObject]()
        self.resultsCollectionView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}

