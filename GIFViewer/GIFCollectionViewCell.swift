//
//  GIFCollectionViewCell.swift
//  GIFViewer
//
//  Created by Daniel Vera on 4/3/16.
//  Copyright © 2016 chuwito. All rights reserved.
//

import UIKit

class GIFCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gifView: UIImageView!
    
    override func prepareForReuse() {
        gifView.image = nil
    }
}
