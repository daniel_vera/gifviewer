//
//  GIFListIC.swift
//  GIFViewer
//
//  Created by Daniel Vera on 7/12/15.
//  Copyright © 2015 chuwito. All rights reserved.
//

import WatchKit
import Foundation


class GIFListIC: WKInterfaceController {

    @IBOutlet var gifListTable: WKInterfaceTable!
    
    var gifListArray: NSArray!
    
    //MARK: life cycle
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        loadGifList()
        loadGIFListTable()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: init functions
    func loadGifList()
    {
        gifListArray = NSArray()
        if let filePath = NSBundle.mainBundle().pathForResource("gif_list", ofType: "plist") {
            gifListArray = NSArray.init(contentsOfFile: filePath)
        }
    }
    
    func loadGIFListTable()
    {
        gifListTable.setNumberOfRows(gifListArray.count, withRowType:"gifRow")
        
        var gifInfo: NSDictionary!
        for index in 0..<gifListTable.numberOfRows {
            if let controller = gifListTable.rowControllerAtIndex(index) as? GIFListItemRowController {
                gifInfo = gifListArray.objectAtIndex(index) as? NSDictionary
                if (gifInfo != nil) {
                    controller.GIFName.setText("\(gifInfo.objectForKey("name")!)")
                    controller.frameName = gifInfo.objectForKey("frameName")! as! String
                    controller.frameCount = gifInfo.objectForKey("frameCount")! as! Int
                }
            }
        }
    }
    
    //MARK: Table
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int)
    {
        let controller = gifListTable.rowControllerAtIndex(rowIndex) as? GIFListItemRowController
        presentControllerWithName("GIFViewer", context: controller!)
    }
    
}
