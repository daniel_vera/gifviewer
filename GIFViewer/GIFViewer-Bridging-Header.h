//
//  GIFViewer-Bridging-Header.h
//  GIFViewer
//
//  Created by Daniel Vera on 4/2/16.
//  Copyright © 2016 chuwito. All rights reserved.
//

#ifndef GIFViewer_Bridging_Header_h
#define GIFViewer_Bridging_Header_h

#import "Giphy-iOS/AXCGiphy.h"
#import "AnimatedGIFImageSerialization/AnimatedGIFImageSerialization.h"

#endif /* GIFViewer_Bridging_Header_h */
